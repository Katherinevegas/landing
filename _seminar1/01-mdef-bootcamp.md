---
title: MDEF Bootcamp
layout: "page"
order: 1
---

![image](http://gitlab.com/MDEF/landing/raw/master/assets/images/ca7bc18b25e9-bootcamp_S.jpg)

### Faculty
Tomas Diez  
Oscar Tomico

### Assistant
Mariana Quintero  
Santi Fuentemilla  
Xavier Dominguez  

### Syllabus and Learning Objectives
The MDEF boot camp is landing and setup workshop that will introduce students to the main ambitions of the master program. The boot camp format will allow students to familiarize themselves with the physical spaces where the program will operate and experiment (classroom, lab, and neighborhood), as well as provide the initial tools to document and share their progress during their studies at IAAC. 

From Wikipedia: *“Boot camps can be governmental being part of the correctional and penal system of some countries. Modeled after military recruit training camps, these programs are based on shock incarceration grounded on military techniques. “*

Do not panic: IAAC is not a correctional facility! And we will only use the best of the boot camp format to facilitate the learning process and the adaptation of the students to the program and the available facilities.

### Presentations from Tutors  
Tomas Diez: Designinig Emergent Futures   

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRYxif4wfJ_ESEG3ib6q-zXAbfHFeN-GF0A9C_HAxtUzprow3TfLueulObPMF21OWzDPoSvXZhsoYkg/embed?start=true&loop=false&delayms=3000" frameborder="0" width="700" height="450" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

Oscar Tomico:  

<iframe src="https://drive.google.com/file/d/1noINdj4Q6zKtdChsdnWEyxBLp-2viB7k/preview" width="700" height="450"></iframe>


Mariana Quintero:  

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQSQLcyVzwgycJRxkMLtTd3TMkMSxX57oDfhTAYIq9-1scO5yAi_peh_B7XzSOUriyZkKudlkdXG9dJ/embed?start=true&loop=false&delayms=3000" frameborder="0" width="700" height="450" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


### Total Duration
Classes: 12 hours (From Tuesday to Friday)
Student work hours: 15 hours
Total hours per student: 27 hours

### Structure and Phases
* *Introduction - Tuesday*     
Who are you, and why are you here?   
How do you see yourself in 10 years?   
Define interest groups  
* *The index of your book - Wednesday*    
How to make your own book?   
Reflective writing  
Organizing information  
Intro to GitLab. Santi and Xavi  
First post: My Future me: how do you see yourself in 10 years?  
* *City Safari - Thursday*    
Tomas Diez presenting his work  
Explore Poblenou. Explore the neighborhood during day and night (with the group of interest)  
Back to IAAC to brief groups about collecting discarded objects in the neighborhood  
Note: document places you believe are interesting to document. Collect things for your working space  
* *City inventory - Friday*    
Oscar Tomico presentating his work  
City inventory. Reference: Make.Works  
· Spaces  
· Skills  
· Materials   
· What is needed vs. what is available  

### Output
To assembly interest groups and working groups. Create your own website. Define your future you. Create a city inventory. Start customizing your space.

### Grading Method
Attitude: 30%  
Documentation: 40%  
Presentation: 30%  

### Bibliography
[Speculative Everything	Anthony Dunne and Fiona Raby](https://mitpress.mit.edu/books/speculative-everything)  
[Adversarial Design	Carl DiSalvo](https://mitpress.mit.edu/books/adversarial-design)  
[Massive Change	Bruce Mau, Jennifer Leonard and Institute without Boundaries](http://www.brucemaudesign.com/work/massive-change)  
[Design for the Real World: Human Ecology and Social Change	Victor Papanek](https://www.amazon.com/Design-%C2%ADReal-%C2%ADWorld-%C2%ADEcology-%C2%ADSocial/dp/0897331532)  
[Liquid Modernity	Zygmunt Bauman](https://www.amazon.com/Liquid-%C2%ADModernity-%C2%ADZygmunt-%C2%ADBauman/dp/0745624103)  
[Who Owns the Future?	Jason Lanier](https://www.amazon.es/gp/product/B008J2AEY8/ref=dp-%C2%ADkindle-%C2%ADredirect?ie=UTF8&btkr=1)  
[This Changes Everything	Naomi Klein](https://thischangeseverything.org/book/)  
[To Save Everything, Click Here: The Folly of Technological Solutionism	Evgeny Morozov](https://www.amazon.com/gp/product/1610393708?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s00)  
[Democratizing Innovation	Eric Von Hippel](https://www.amazon.com/gp/product/0262720477?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s01)  
[Cradle to Cradle: Remaking the Way We Make Things	Michael Braungart, William McDonough](http://www.amazon.com/gp/product/0865475873?psc=1&redirect=true&ref_=od_aui_detailpages00)  
[Macrowikinomics: New Solutions for a Connected Planet	Don Tapscott, Anthony D. Williams](http://www.amazon.com/gp/product/1591844282?psc=1&redirect=true&ref_=od_aui_detailpages00)  
[The Third Industrial Revolution: How Lateral Power Is Transforming Energy, the Economy, and the World	Jeremy Rifkin](http://www.amazon.com/gp/product/0230115217?psc=1&redirect=true&ref_=od_aui_detailpages00g)  

### Background Research Material
[Notion-Reading List](https://www.notion.so/mdef/e3369a460eed46be97754e76763d4175?v=d9178e4bd0e34a8aa88b52629798aa0c)

### Requirements for the Students
Personal computer, smart phone (install preferred mapping app), camera (optional), notebook. 

### Infrastructure Needs
Classroom 201. Storage of collected objects.

### TOMAS DIEZ
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/0302393192f2-Screenshot_2018_09_10_at_12.37.01_20180912191624503.jpg)

### Email Address
<tomasdiez@iaac.net>

### Personal Website
[Tomas Diez](http://tomasdiez.com)

### Twitter Account
@tomasdiez

### Facebook Profile
tomasdiez77

### Professional BIO
Tomas Diez is a Venezuelan Urbanist specialized in digital fabrication and its implications on the future cities and society. He is the co-founder of Fab Lab Barcelona, leads the Fab City Research Laboratory, and is a founding partner of the Fab City Global Initiative. He is the director of the Master in Design for Emergent Futures at the Institute for Advanced Architecture of Catalonia (IAAC) in Barcelona, where he is faculty in urban design and digital fabrication. Tomas is co-founder of other initiatives such as Smart Citizen (open source tools for citizen engagement), Fab City (locally productive, globally connected cities), Fablabs.io (the listing of fab labs in the world), and StudioP52 (art and design space in Barcelona).