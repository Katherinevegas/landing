---
title: Students' Projects
icon: fa-th
order: 9
---

<!-- DO NOT CHANGE THIS! Instead edit the file /_data/student-projects -->
<div class="row">
{% for project in site.data.student-projects %}
    <div class="3u 12u$(mobile)">
      <div class="item student-project">
        <h6>{{ project.name }} </h6>
        <a href="{{ project.url }}" class="image fit"><img src="{{ project.image | relative_url }}"></a>
      </div>
    </div>
{% endfor %}
</div>
